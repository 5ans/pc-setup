#!/bin/bash

shopt -s expand_aliases
source $HOME/.bash_aliases

if [ -z $1 ]; then
    echo "must pass a platform e.g. \"ceres\""
    exit -1
else
    PLATFORM=$1
fi


if [ -z $2 ]; then
    OUT_FOLDER=./otatest-artifacts
else
    OUT_FOLDER=$2
fi

echo "Artifacts will be placed in ${OUT_FOLDER}"
echo ""
mkdir -p ${OUT_FOLDER}

if [ $? -ne 0 ]; then
    echo "could not create ${OUT_FOLDER}"
    exit $?
fi

mkdir -p ${OUT_FOLDER}/inputs
mkdir -p ${OUT_FOLDER}/cbx
mkdir -p ${OUT_FOLDER}/bto

################################### FW INPUT ###################################
build_fw() {
    pushd $WIGGLE_REPO_PATH > /dev/null
    if [ $? -ne 0 ]; then
        exit $?
    fi
    rm -rf ./build/${PLATFORM}_main/
    make -f ./ewp/${PLATFORM}.mk -j > /dev/null
    if [ $? -ne 0 ]; then
        popd > /dev/null
        exit $?
    fi
    popd > /dev/null
}
BUILD_FOLDER="${WIGGLE_REPO_PATH}/build/${PLATFORM}_main"

# build the old fw and copy to inputs
build_fw
if [ $? -ne 0 ]; then
    echo "old build failed"
    exit $?
fi
cp ${BUILD_FOLDER}/cust_app_update.bin ${OUT_FOLDER}/inputs/fw_old.bin
if [ $? -ne 0 ]; then
    echo "could not copy ${BUILD_FOLDER}/cust_app_update.bin"
    exit $?
fi

# Get the current engineering version
CUR_ENGVER=$(cat ${WIGGLE_REPO_PATH}/sdk/whdl_version.h |\
             grep '#define SDK_ENGINEERING_VER' |\
             sed -E 's/^.*"([0-9]+)".*$/\1/')
# increment the engineering version and modify in the whdl_version.h
NEW_ENGVER=$((${CUR_ENGVER}+1))
sed -i -E "s/(#define SDK_ENGINEERING_VER.*)\"[0-9]+\"(.*$)/\1\"${NEW_ENGVER}\"\2/g" \
    ${WIGGLE_REPO_PATH}/sdk/whdl_version.h

# build the new fw and copy to inputs
build_fw
if [ $? -ne 0 ]; then
    echo "new build failed"
    exit $?
fi
cp ${BUILD_FOLDER}/cust_app_update.bin ${OUT_FOLDER}/inputs/fw_new.bin

# Replace the engineering version back to the original
sed -i -E "s/(#define SDK_ENGINEERING_VER.*)\"[0-9]+\"(.*$)/\1\"${CUR_ENGVER}\"\2/g" \
    ${WIGGLE_REPO_PATH}/sdk/whdl_version.h



################################# SCRIPT INPUT #################################
build_script() {
    pushd ${WIGGLE_REPO_PATH}/scripts > /dev/null
    ./build.sh event > /dev/null
    if [ $? -ne 0 ]; then
        popd > /dev/null
        exit $?
    fi
    popd > /dev/null
}
# save the OG verstion
mkdir -p ${WIGGLE_REPO_PATH}/scripts/tmp
cp ${WIGGLE_REPO_PATH}/scripts/event.* ${WIGGLE_REPO_PATH}/scripts/tmp

# use pack(1, 2) in all places in script
sed -i -E "s/^(.*)pack\([0-9]+, [0-9]+\)(.*$)/\1pack\(1, 1\)\2/g" \
    ${WIGGLE_REPO_PATH}/scripts/event.p

# use 5 second timer for periodics
sed -i -E "s/^(.*)timer_set_ms\(1, 1800000\);(.*$)/\1timer_set_ms\(1, 5000\);\2/g" \
    ${WIGGLE_REPO_PATH}/scripts/event.p

# build and move the old script
build_script
if [ $? -ne 0 ]; then
    echo "old script build failed"
    exit $?
fi
cp ${WIGGLE_REPO_PATH}/scripts/event.amx ${OUT_FOLDER}/inputs/script_old.amx

# use 10 second timer for periodics
sed -i -E "s/^(.*)timer_set_ms\(1, 5000\);(.*$)/\1timer_set_ms\(1, 10000\);\2/g" \
    ${WIGGLE_REPO_PATH}/scripts/event.p

# build and move the new script
build_script
if [ $? -ne 0 ]; then
    echo "new script build failed"
    exit $?
fi
cp ${WIGGLE_REPO_PATH}/scripts/event.amx ${OUT_FOLDER}/inputs/script_new.amx

# cleanup, replace event script files with saved copies
mv ${WIGGLE_REPO_PATH}/scripts/tmp/*.* ${WIGGLE_REPO_PATH}/scripts
rmdir ${WIGGLE_REPO_PATH}/scripts/tmp
rm ${WIGGLE_REPO_PATH}/scripts/event.zlib.*



################################### ECR INPUT ##################################
# echo "1,2,ffff0000,ffff0004" > ${OUT_FOLDER}/inputs/otatest.ecr
echo "1,1A,ffff0000,ffff0004,fffe0000,fffe0001,00040000,00040001,00070001,00070004,00000000,00000001,00000003,00010000,00010001,00010003,00010004,00010005,00010006,00020000,00020001,00030000,00030001,00060000,00060001,00090000,000A0000,000B0000" > ${OUT_FOLDER}/inputs/otatest.ecr

python3 ${WIGGLE_REPO_PATH}/tools/genecr/genecr.py \
    "${OUT_FOLDER}/inputs/otatest.ecr" \
    "${OUT_FOLDER}/inputs/ecr.out"
rm ${OUT_FOLDER}/inputs/otatest.ecr



################################ PROCESS OUTPUTS ###############################
echo "raw input file sha's:"
sha256sum ${OUT_FOLDER}/inputs/*
echo ""

${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx ${OUT_FOLDER}/inputs/ecr.out > /dev/null
${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx ${OUT_FOLDER}/inputs/fw_new.bin > /dev/null
${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx ${OUT_FOLDER}/inputs/script_new.amx > /dev/null

${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx -c zlib ${OUT_FOLDER}/inputs/ecr.out > /dev/null
${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx -c zlib ${OUT_FOLDER}/inputs/fw_old.bin > /dev/null
${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx -c zlib ${OUT_FOLDER}/inputs/script_old.amx > /dev/null

${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx -c bsdiff \
    -d ${OUT_FOLDER}/inputs/fw_old.bin ${OUT_FOLDER}/inputs/fw_new.bin > /dev/null
${WIGGLE_REPO_PATH}/tools/mkbins/mkcbx -c bsdiff \
    -d ${OUT_FOLDER}/inputs/script_old.amx ${OUT_FOLDER}/inputs/script_new.amx > /dev/null

mv ${OUT_FOLDER}/inputs/*.cbx ${OUT_FOLDER}/cbx
rm ${OUT_FOLDER}/inputs/*.pad

echo "cbx file sha's:"
sha256sum ${OUT_FOLDER}/cbx/*
echo ""

${WIGGLE_REPO_PATH}/tools/mkbins/mkbto -o \
    ${OUT_FOLDER}/bto/test1-new-raw \
    9=${OUT_FOLDER}/cbx/ecr.cbx \
    6=${OUT_FOLDER}/cbx/script_new.cbx \
    2=${OUT_FOLDER}/cbx/fw_new.cbx > /dev/null

${WIGGLE_REPO_PATH}/tools/mkbins/mkbto -c zlib -o \
    ${OUT_FOLDER}/bto/test2-old-zlib \
    9=${OUT_FOLDER}/cbx/ecr.cbx \
    6=${OUT_FOLDER}/cbx/script_old.zlib.cbx \
    2=${OUT_FOLDER}/cbx/fw_old.zlib.cbx > /dev/null

${WIGGLE_REPO_PATH}/tools/mkbins/mkbto -c zlib -o \
    ${OUT_FOLDER}/bto/test3-new-bsdiff \
    9=${OUT_FOLDER}/cbx/ecr.cbx \
    6=${OUT_FOLDER}/cbx/script_old-script_new.bsdiff.cbx \
    2=${OUT_FOLDER}/cbx/fw_old-fw_new.bsdiff.cbx > /dev/null

echo "bto file sha's:"
sha256sum ${OUT_FOLDER}/bto/*
echo ""
