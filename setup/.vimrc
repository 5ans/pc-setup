" ============================================================================
"   ==================== VUNDLE PLUGIN MANAGER SETTINGS ====================
" ============================================================================
set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin('~/.vim/bundle/')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" optional plugins below
Plugin 'powerline/powerline-fonts'
Plugin 'kien/ctrlp.vim'
Plugin 'FelikZ/ctrlp-py-matcher'
Plugin 'scrooloose/nerdtree'
Plugin 'flazz/vim-colorschemes'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'jacoborus/tender.vim'
Plugin 'gosukiwi/vim-atom-dark'
Plugin 'tomasiser/vim-code-dark'
Plugin 'scrooloose/nerdcommenter' 
Plugin 'kchmck/vim-coffee-script'
Plugin 'altercation/vim-colors-solarized'
Plugin 'morhetz/gruvbox'
Plugin 'mileszs/ack.vim'
Plugin 'airblade/vim-rooter'
Plugin 'majutsushi/tagbar'
Plugin 'brookhong/cscope.vim'
Plugin 'tpope/vim-dispatch'
" Plugin 'szw/vim-tags'
Plugin 'tpope/vim-fugitive'
Plugin 'jceb/vim-orgmode'
Plugin 'mphe/grayout.vim'
"Plugin 'valloric/youcompleteme'
"Plugin 'scrooloose/syntastic'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"set shell=powershell
"set shellcmdflag=-command"

" ============================================================================
"   ========================= LOAD DEFAULT SETTINGS ========================
" ============================================================================
if v:progname =~? "evim"
  finish
endif
" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim
if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif
if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif
" Only do this part when compiled with support for autocommands.
if has("autocmd")
  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!
  " For all text files set 'textwidth' to 80 characters.
  autocmd FileType text setlocal textwidth=80
  augroup END
else
  set autoindent		" always set autoindenting on
endif " has("autocmd")

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
  packadd! matchit
endif

" ============================================================================
"   ======================== VIM TERMINAL SETTINGS ========================
" ============================================================================
set termwinsize=
let g:term_bnum = -1
let g:term_winid = -1

function! TermToggle(caller_win_id)
  if bufexists(g:term_bnum)
    let tdeleted = 1
    call bufnr(g:term_bnum)->term_sendkeys("\<End>\<C-U>exit\<CR>")
    exe "bw! ".g:term_bnum
  else
    let tdeleted = 0
    bo term
    let g:term_bnum = bufnr()
    let g:term_winid = win_getid()
    call term_setsize(g:term_bnum, 10, 0)
  endif
  if a:caller_win_id == g:sterm_winid
    call win_gotoid(g:sterm_winid)
    norm "ii"
    if tdeleted == 0
      call win_gotoid(g:term_winid)
    endif
  endif
endfunc

nmap <F10> :call TermToggle()<CR>

" stuff we want to call every time a terminal opens
function! TermACMD()
  if &buftype == 'terminal'
    setlocal bufhidden=hide
    setlocal nobuflisted
    setlocal wfh
    setlocal wfw
  endif
endfunc
au TerminalOpen * call TermACMD()

tnoremap <F1> <C-W>N
nmap <F10> :call TermToggle(0)<CR>
tnoremap <F10> <C-W>N:call TermToggle(win_getid())<CR>


" ============================================================================
"   ====================== VIM SERIAL TERM SETTINGS ========================
" ============================================================================

let g:sterm_bnum = -1
let g:sterm_winid = -1

function! STermToggle(caller_win_id)
  if bufexists(g:sterm_bnum)
    let tdeleted = 1
    call bufnr(g:sterm_bnum)->term_sendkeys("\<C-A>:quit\<CR>")
    sleep 200m
    call bufnr(g:sterm_bnum)->term_sendkeys("\<End>\<C-U>exit\<CR>")
    exe "bw! ".g:sterm_bnum
  else
    let tdeleted = 0
    bo vert term
    let g:sterm_bnum = bufnr()
    let g:sterm_winid = win_getid()
    call term_setsize(g:sterm_bnum, 0, 80)
  endif
  if a:caller_win_id == g:term_winid
    call win_gotoid(g:term_winid)
    norm "ii"
    if tdeleted == 0
      call win_gotoid(g:sterm_winid)
    endif
  endif
endfunc

nmap <F11> :call STermToggle(0)<CR>
tnoremap <F11> <C-W>N:call STermToggle(win_getid())<CR>

" ============================================================================
"   ======================== VIM BEHAVIOR SETTINGS ========================
" ============================================================================

"============ PASSIVE SKILLZ ============

" allow a new buffer to be loaded without saving the current buffer
set hidden

" setup whitespace characters for display with :set list (nolist to un-display)
"set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣

" remove all gui elements, no toolbar or scrollbars
set guioptions-=T
set guioptions-=m
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L
set guioptions-=b
set guioptions-=F

" no windows chime or flash
set belloff=all
"set belloff=

" Save backup files in temp dir
set backup
set undodir=~/temp
set backupdir=~/temp
set directory=~/temp
set writebackup

" Set completion to stop scanning included files
setglobal complete-=i
set complete-=i

" Reanimate paths
set path+=src
set path+=libraries

" indent options
autocmd BufNewFile,BufRead,BufFilePost *.c,*.h set cindent
" c indent option: put switch and corresponding case at same indent level
set cino+=:0
" c indent option: line up with open-paren on next line
" function_name(arg1, long_name_for_arg2,
"               long_name_for_arg3);
set cino+=(0
" c indent option: indent args by 4 when open-paren ends the line
" a_really_long_function_name_that_has_long_args(
"     next_arg_is_indented_4_spaces_from_function_start,
"     as_opposed_to_aligned_with_open_paren);
set cino+=W4
"use code folding
set foldmethod=indent   
set foldnestmax=10
set foldlevel=6
set nofoldenable
"open vim expanded to full screen (windows only)
"au GUIEnter * simalt ~x
" set lines=999 columns=999
"vertical splits always go to the right
set splitright
"jump to already opened buffer, if it is specified
set switchbuf=useopen
"max columns line
set textwidth=80
set colorcolumn=+1
"highlight crap space
let c_space_errors = 1
"set tabs to use 4 spaces
set expandtab
set shiftwidth=4
set softtabstop=4
" detect .h files as 'c' instead of 'cpp'.  Useful for plugins that depend on
" filetype (NERD_commenter is the one I care about)
let g:c_syntax_for_h=1
"set ignore filetypes/paths we know we don't care about
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe,*.i,*.o,*.s,*.lst,*.pbi*,*.pbi.cout
"use ripgrep for file grepping
if executable("rg")
    set grepprg=rg\ --vimgrep\ --no-heading
    set grepformat=%f:%l:%c:%m,%f:%l:%m
endif
" cscope settings
if has("cscope")
    "point to the executable
    set csprg=C:/cygwin64/bin/cscope
    "quickfix settings
    set cscopequickfix=s-,c-,d-,i-,t-,e-,a-
    "cscope tag order, search cscope database before tags file
    set csto=1
endif

filetype plugin on

"set ignorecase
set smartcase
"============ ACTIVE SKILLS ============

"cscope mappings
    "call interactive cscope
nmap <leader>S :call CscopeFindInteractive(expand('<cword>'))<CR>
    "call cscope with the corresponding type
nmap <leader>ss :call CscopeFind('s',expand('<cword>'))<CR>
nmap <leader>sc :call CscopeFind('c',expand('<cword>'))<CR>
nmap <leader>sd :call CscopeFind('d',expand('<cword>'))<CR>
nmap <leader>sf :call CscopeFind('f',expand('<cword>'))<CR>
nmap <leader>st :call CscopeFind('t',expand('<cword>'))<CR>
    "setup cscope shortcut, enter text to search
nmap <leader>w :call CscopeFind('s','')<Left><Left>

"always open help in vertical window
cnoreabbrev <expr> help ((getcmdtype() is# ':' && getcmdline() is# 'help')?('vert help'):('help'))
cnoreabbrev <expr> h ((getcmdtype() is# ':' && getcmdline() is# 'h')?('vert help'):('h'))

"call ack when cursor on word in normal mode
nnoremap <leader>a :Ack! -F <cword><cr>
"call ack on anything selected in visual mode
vnoremap <leader>a y:Ack! -F "<C-r>""<CR>
"setup ack shortcut, enter text to search
nmap <leader>q :Ack 

"capitalization helpers
    "capitalize word under cursor in normal mode
nmap <leader>u gUiw
    "capitalize whatever was just typed while still in insert mode
imap <C-u> <Esc>lmubgU`u`ui
    "paste from the " reg while in insert mode
inoremap <C-l> <C-o>P

"auto center text
nmap <leader><space><space> 0d$:let @"=CenterText(@")<CR>p

"function key mappings
    "save current file
nmap <F2> :w<CR>
    "delete currently selected buffer
nmap <F3> :bp<CR>:bd#<CR>
    "close the current window
nmap <F4> :clo<CR>
    "refresh caches and tag/database files
nmap <F5> :CtrlPClearCache<CR>:CtrlP<CR><Esc>:call UpdateCTags()<CR>
    "Build w/IAR
" nmap <F6> EMPTY/UNUSED
    "close fold at this indent
nmap <F7> za 
    "open fold at this indent
nmap <F8> zo 
    "open NERDTree
nmap <F9> :NERDTreeToggle<CR><C-W>=

    " F10/F11 defined above in VIM TERMINAL SETTINGS

    "toggle the tagbar on/off
nmap <F12> :TagbarToggle<CR> 

" transparency toggle, doesn't work yet
let t:is_transparent = 0                     
function! Toggle_transparent_background()                      
  if t:is_transparent == 0                   
    hi Normal guibg=#111111 ctermbg=black                     
    let t:is_transparent = 1
  else
    hi Normal guibg=NONE ctermbg=NONE                    
    let t:is_transparent = 0                        
  endif                    
endfunction               
nnoremap <C-x><C-t> :call Toggle_transparent_background()<CR>



"useful snippets
    " quick ifndef statement for the beginning of header files
nmap <Leader>hs 0i#ifndef<Space><Esc>yy2po<Esc>jo<Esc>klcwendif<Space>/*<Esc>A<Space>*/<Esc>2k2bcwdefine<Esc>j
    " turn #if into equivalent #endif with commented-out test
nmap <Leader>hh :s/if/endif/<CR> :s/(/\/\* (/<CR> :s/)/) \*\//<CR>
    " 
nmap <Leader>dd odebug_print(0x800, "\r\n");<Esc>



" ============================================================================
"   ======================== VIM APPEARANCE SETTINGS =======================
" ============================================================================

" enable syntax highlighting
syntax enable
" enable dark background, this affects some colorschemes
set background=dark
" font, must be a patched powerline font for your statusline to look good
set guifont=Ubuntu\ Mono\ derivative\ Powerline\ Bold\ 12
" encoding...
set encoding=utf-8
" display line numbers
set number
" syntax recognition for pawn scripts
au BufRead,BufNewFile *.inc set filetype=c
au BufRead,BufNewFile *.i set filetype=c
au BufRead,BufNewFile *.p set filetype=c

"============ COLORSCHEMES =============

" let g:solarized_termtrans=1
" colorscheme solarized

" colorscheme luna

" colorscheme hornet

" colorscheme vivacious

" colorscheme headache

" colorscheme badwolf

" colorscheme hybrid

" colorscheme onedark

" colorscheme tender

" colorscheme hybrid_material
" let g:airline_theme='hybridline'

" colorscheme atom-dark
" let g:airline_theme='codedark'

" colorscheme Monokai
" let g:airline_theme='molokai'

" colorscheme PaperColor

colorscheme gruvbox
set termguicolors
"=======================================

" ============================================================================
"   ========================= VIM PLUGIN SETTINGS ==========================
" ============================================================================
 
"============== VIM-ROOTER ===============
let g:rooter_patterns = ['.git']

"============== VIM-TAGS ===============
let g:vim_tags_auto_generate = 0
let g:vim_tags_use_vim_dispatch = 0
" let g:vim_tags_project_tags_command = "{CTAGS} -R --langmap=C:.c.h.p.i.inc --languages=C --exclude=tools {DIRECTORY} /opt/microchip/mplabx/v6.10/packs/Microchip/ATtiny_DFP/3.0.151/include/avr/"

"=============== CSCOVE ================
let g:cscope_cmd = 'C:/cygwin64/bin/cscope'
let g:cscope_interested_files = '\.c$\|\.h$'
let g:cscope_ignored_dir = 'tools$'
let g:cscope_silent = 1
let g:cscope_open_location = 1
let g:cscope_auto_update = 0

"=========== NERD COMMENTER ============
let g:NERDCompactSexyComs = 1
let g:NERDSpaceDelims = 1

"=============== CTRL-P ================
let g:ctrlp_working_path_mode = 'rw'
" let g:ctrlp_by_filename = 1
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
if executable('rg')
    set grepprg=rg\ --color=never
    let g:ctrlp_user_command = 'rg %s --files --color=never --glob ""'
    let g:ctrlp_use_caching = 1
elseif executable('fd')
    " Use fd for ctrlp.
    let g:ctrlp_user_command = 'fd -c never "" "%s"'
    let g:ctrlp_use_caching = 1
endif
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(tools)$',
  \ }

"================= ACK =================
" never jump immediately to first Ack result
cnoreabbrev Ack Ack!
if executable('rg')
" use ripgrep for ack
    "let g:ackprg = 'rg --vimgrep --no-heading -F -E UTF-8 -t c --no-sort-files -g !"tools/**" -g !"ewp/**" -g !"build/**" -g !"libraries/aws_iot/**" -g !"libraries/mbedtls/**" --type-add "c:*.tbc" --type-add "c:*.tbs"'
    let g:ackprg = 'rg --vimgrep --no-heading -E UTF-8 -t c --no-sort-files -g !"tools/**" -g !"ewp/**" -g !"build/**" --type-add "c:*.p" --type-add "c:*.i" --type-add "c:*.inc"'
endif

"============= AIR-LINE ================
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
"airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = 'Ɇ'
let g:airline_symbols.whitespace = 'Ξ'

"airline - tabline
let g:airline#extensions#tabline#ignore_bufadd_pat =
  \ 'gundo|undotree|vimfiler|tagbar|nerd_tree|startify|!'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>- <Plug>AirlineSelectPrevTab
nmap <leader>= <Plug>AirlineSelectNextTab

"airline - syntastic
let g:airline#extensions#syntastic#stl_format_err = 1
let g:airline#extensions#syntastic#stl_format_warn = 1

"airline - tagbar
let g:airline#extensions#tagbar#enabled = 1

"airline - ctrlp
let g:airline#extensions#ctrlp#color_template = 'insert'
let g:airline#extensions#ctrlp#color_template = 'normal'
let g:airline#extensions#ctrlp#color_template = 'visual'
let g:airline#extensions#ctrlp#color_template = 'replace'
let g:airline#extensions#ctrlp#show_adjacent_modes = 1

"============== SYNTASTIC ==============
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
"let g:syntastic_c_checkers = ["clang_check","clang_tidy","gcc"]

"============ YOUCOMPLETEME ============
"let g:ycm_register_as_syntastic_checker = 1 "default 1
"let g:Show_diagnostics_ui = 1 "default 1

"will put icons in Vim's gutter on lines that have a diagnostic set.
"Turning this off will also turn off the YcmErrorLine and YcmWarningLine
"highlighting
"let g:ycm_global_ycm_extra_conf = '$USERPROFILE/vimfiles/.ycm_extra_conf.py'
"let g:ycm_enable_diagnostic_signs = 1
"let g:ycm_enable_diagnostic_highlighting = 0
"let g:ycm_always_populate_location_list = 1 "default 0
"let g:ycm_open_loclist_on_ycm_diags = 1 "default 1
"let g:ycm_complete_in_strings = 1 "default 1
"let g:ycm_collect_identifiers_from_tags_files = 1 "default 0
"let g:ycm_path_to_python_interpreter = '' "default ''
"let g:ycm_server_use_vim_stdout = 0 "default 0 (logging to console)
"let g:ycm_server_log_level = 'info' "default info
"let g:ycm_confirm_extra_conf = 1
"let g:ycm_goto_buffer_command = 'same-buffer' "[ 'same-buffer', 'horizontal-split', 'vertical-split', 'new-tab' ]
"let g:ycm_filetype_whitelist = { '*': 1 }
"let g:ycm_key_invoke_completion = '<C-Space>'

" set diffexpr=MyDiff()
" function! MyDiff()
  " let opt = '-a --binary '
  " if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  " if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  " let arg1 = v:fname_in
  " if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  " let arg1 = substitute(arg1, '!', '\!', 'g')
  " let arg2 = v:fname_new
  " if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  " let arg2 = substitute(arg2, '!', '\!', 'g')
  " let arg3 = v:fname_out
  " if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  " let arg3 = substitute(arg3, '!', '\!', 'g')
  " if $VIMRUNTIME =~ ' '
    " if &sh =~ '\<cmd'
      " if empty(&shellxquote)
        " let l:shxq_sav = ''
        " set shellxquote&
      " endif
      " let cmd = '"' . $VIMRUNTIME . '\diff"'
    " else
      " let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    " endif
  " else
    " let cmd = $VIMRUNTIME . '\diff'
  " endif
    " let cmd = substitute(cmd, '!', '\!', 'g')
    " silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
    " if exists('l:shxq_sav')
      " let &shellxquote=l:shxq_sav
  " endif
" endfunction

"tag auto updating
let g:taglock = 0

function! TagUpdateUnlock(timer)
  let g:taglock = 0
endfunc

function! UpdateCTags()
  if getcwd() != $HOME
    if g:taglock == 0
      let g:taglock = 1
      silent Dispatch! cat ./.vim/ctag_paths.txt | envsubst | ctags --options=./.vim/ctag_options.txt
      let g:tagtimer = timer_start(10000, 'TagUpdateUnlock')
    endif
  endif
endfunc

au BufWritePost *.c,*.h,*.cpp,*.hpp call UpdateCTags()
"tag auto updating end
 
function! CenterText(input)
  "first get the real words beginning and end
  let len = strlen(a:input)
  let fcidx = match(a:input, '\h')
  let idx = len
  while idx > 0
    let lcidx = match(a:input, '\h', idx)
    if lcidx == idx
      break
    endif
    let idx = idx-1
  endwhile
  if lcidx <= fcidx
    return -1
  endif
  let wordspan = lcidx - fcidx + 1

  "now get the leading and trailing whitespace beginning and end
  let idx = fcidx-1
  while idx > 0
    let lwfidx = match(a:input, '\s', idx)
    if lwfidx != idx
      break
    endif
    let idx = idx-1
  endwhile

  let idx = len
  while idx > 0
    let twlidx = match(a:input, '\s', idx)
    if twlidx == idx
      break
    endif
    let idx = idx-1
  endwhile
  if twlidx <= lwfidx
    return -1
  endif

  let mainwords = strpart(a:input, fcidx, wordspan)
  let n_startchars = lwfidx
  let startchars = strpart(a:input, 0, n_startchars)
  let n_endchars = len-twlidx-1
  let endchars = strpart(a:input, twlidx+1, n_endchars)

  let n_wschars = len - wordspan - n_startchars - n_endchars
  let spaces = ''
  for idx in range((n_wschars/2))
    let spaces = spaces . ' '
  endfor
  if n_wschars % 2
    let extra = ' '
  else
    let extra = ''
  endif
  let output = startchars . spaces . mainwords . spaces . extra . endchars
  return output
endfunc


"window width manipulation (must go last for some reason)
set winaltkeys=no
nmap <A-,> :2winc <<CR>
nmap <A-.> :2winc ><CR>
nmap <A-m> :2winc -<CR>
nmap <A-n> :2winc +<CR>
nmap <Esc>, :2winc <<CR>
nmap <Esc>. :2winc ><CR>
nmap <Esc>m :2winc -<CR>
nmap <Esc>n :2winc +<CR>

" After everything else, allow users to override any of this with their own
" specific settings
if filereadable(expand("~/_user.vim"))
  source ~/_user.vim
endif

