#ifndef QUECTEL_EFS_H
#define QUECTEL_EFS_H
#include "quectel_include.h"

#define DIAG_SUBSYS_CMD_F 75

typedef enum
{
    DIAG_SUBSYS_OEM = 0,    /* Reserved for OEM use */
    DIAG_SUBSYS_ZREX = 1,   /* ZREX */
    DIAG_SUBSYS_SD = 2,     /* System Determination */
    DIAG_SUBSYS_BT = 3,     /* Bluetooth */
    DIAG_SUBSYS_WCDMA = 4,  /* WCDMA */
    DIAG_SUBSYS_HDR = 5,    /* 1xEvDO */
    DIAG_SUBSYS_DIABLO = 6, /* DIABLO */
    DIAG_SUBSYS_TREX = 7,   /* TREX - Off-target testing environments */
    DIAG_SUBSYS_GSM = 8,    /* GSM */
    DIAG_SUBSYS_UMTS = 9,   /* UMTS */
    DIAG_SUBSYS_HWTC = 10,  /* HWTC */
    DIAG_SUBSYS_FTM = 11,   /* Factory Test Mode */
    DIAG_SUBSYS_REX = 12,   /* Rex */
    DIAG_SUBSYS_OS = DIAG_SUBSYS_REX,
    DIAG_SUBSYS_GPS = 13,               /* Global Positioning System */
    DIAG_SUBSYS_WMS = 14,               /* Wireless Messaging Service (WMS, SMS) */
    DIAG_SUBSYS_CM = 15,                /* Call Manager */
    DIAG_SUBSYS_HS = 16,                /* Handset */
    DIAG_SUBSYS_AUDIO_SETTINGS = 17,    /* Audio Settings */
    DIAG_SUBSYS_DIAG_SERV = 18,         /* DIAG Services */
    DIAG_SUBSYS_FS = 19,                /* File System - EFS2 */
    DIAG_SUBSYS_PORT_MAP_SETTINGS = 20, /* Port Map Settings */
    DIAG_SUBSYS_MEDIAPLAYER = 21,       /* QCT Mediaplayer */
    DIAG_SUBSYS_QCAMERA = 22,           /* QCT QCamera */
    DIAG_SUBSYS_MOBIMON = 23,           /* QCT MobiMon */
    DIAG_SUBSYS_GUNIMON = 24,           /* QCT GuniMon */
    DIAG_SUBSYS_LSM = 25,               /* Location Services Manager */
    DIAG_SUBSYS_QCAMCORDER = 26,        /* QCT QCamcorder */
    DIAG_SUBSYS_MUX1X = 27,             /* Multiplexer */
    DIAG_SUBSYS_DATA1X = 28,            /* Data */
    DIAG_SUBSYS_SRCH1X = 29,            /* Searcher */
    DIAG_SUBSYS_CALLP1X = 30,           /* Call Processor */
    DIAG_SUBSYS_APPS = 31,              /* Applications */
    DIAG_SUBSYS_SETTINGS = 32,          /* Settings */
    DIAG_SUBSYS_GSDI = 33,              /* Generic SIM Driver Interface */
    DIAG_SUBSYS_UIMDIAG = DIAG_SUBSYS_GSDI,
    DIAG_SUBSYS_TMC = 34, /* Task Main Controller */
    DIAG_SUBSYS_USB = 35, /* Universal Serial Bus */
    DIAG_SUBSYS_PM = 36,  /* Power Management */
    DIAG_SUBSYS_DEBUG = 37,
    DIAG_SUBSYS_QTV = 38,
    DIAG_SUBSYS_CLKRGM = 39, /* Clock Regime */
    DIAG_SUBSYS_DEVICES = 40,
    DIAG_SUBSYS_WLAN = 41,            /* 802.11 Technology */
    DIAG_SUBSYS_PS_DATA_LOGGING = 42, /* Data Path Logging */
    DIAG_SUBSYS_PS = DIAG_SUBSYS_PS_DATA_LOGGING,
    DIAG_SUBSYS_MFLO = 43, /* MediaFLO */
    DIAG_SUBSYS_DTV = 44,  /* Digital TV */
    DIAG_SUBSYS_RRC = 45,  /* WCDMA Radio Resource Control state */
    DIAG_SUBSYS_PROF = 46, /* Miscellaneous Profiling Related */
    DIAG_SUBSYS_TCXOMGR = 47,
    DIAG_SUBSYS_NV = 48, /* Non Volatile Memory */
    DIAG_SUBSYS_AUTOCONFIG = 49,
    DIAG_SUBSYS_PARAMS = 50, /* Parameters required for debugging subsystems */
    DIAG_SUBSYS_MDDI = 51,   /* Mobile Display Digital Interface */
    DIAG_SUBSYS_DS_ATCOP = 52,
    DIAG_SUBSYS_L4LINUX = 53,              /* L4/Linux */
    DIAG_SUBSYS_MVS = 54,                  /* Multimode Voice Services */
    DIAG_SUBSYS_CNV = 55,                  /* Compact NV */
    DIAG_SUBSYS_APIONE_PROGRAM = 56,       /* apiOne */
    DIAG_SUBSYS_HIT = 57,                  /* Hardware Integration Test */
    DIAG_SUBSYS_DRM = 58,                  /* Digital Rights Management */
    DIAG_SUBSYS_DM = 59,                   /* Device Management */
    DIAG_SUBSYS_FC = 60,                   /* Flow Controller */
    DIAG_SUBSYS_MEMORY = 61,               /* Malloc Manager */
    DIAG_SUBSYS_FS_ALTERNATE = 62,         /* Alternate File System */
    DIAG_SUBSYS_REGRESSION = 63,           /* Regression Test Commands */
    DIAG_SUBSYS_SENSORS = 64,              /* The sensors subsystem */
    DIAG_SUBSYS_FLUTE = 65,                /* FLUTE */
    DIAG_SUBSYS_ANALOG = 66,               /* Analog die subsystem */
    DIAG_SUBSYS_APIONE_PROGRAM_MODEM = 67, /* apiOne Program On Modem Processor */
    DIAG_SUBSYS_LTE = 68,                  /* LTE */
    DIAG_SUBSYS_BREW = 69,                 /* BREW */
    DIAG_SUBSYS_PWRDB = 70,                /* Power Debug Tool */
    DIAG_SUBSYS_CHORD = 71,                /* Chaos Coordinator */
    DIAG_SUBSYS_SEC = 72,                  /* Security */
    DIAG_SUBSYS_TIME = 73,                 /* Time Services */
    DIAG_SUBSYS_Q6_CORE = 74,              /* Q6 core services */
    DIAG_SUBSYS_COREBSP = 75,              /* CoreBSP */
    /* Command code allocation:
        [0 - 2047]	- HWENGINES
        [2048 - 2147]	- MPROC
        [2148 - 2247]	- BUSES
        [2248 - 2347]	- USB
        [2348 - 2447]   - FLASH
        [2448 - 3447]   - UART
        [3448 - 3547]   - PRODUCTS
        [3547 - 65535]	- Reserved
    */

    DIAG_SUBSYS_MFLO2 = 76, /* Media Flow */
    /* Command code allocation:
        [0 - 1023]       - APPs
        [1024 - 65535]   - Reserved
    */
    DIAG_SUBSYS_ULOG = 77,    /* ULog Services */
    DIAG_SUBSYS_APR = 78,     /* Asynchronous Packet Router (Yu, Andy)*/
    DIAG_SUBSYS_QNP = 79,     /*QNP (Ravinder Are , Arun Harnoor)*/
    DIAG_SUBSYS_STRIDE = 80,  /* Ivailo Petrov */
    DIAG_SUBSYS_OEMDPP = 81,  /* to read/write calibration to DPP partition */
    DIAG_SUBSYS_Q5_CORE = 82, /* Requested by ADSP team */
    DIAG_SUBSYS_USCRIPT = 83, /* core/power team USCRIPT tool */
    DIAG_SUBSYS_NAS = 84,     /* Requested by 3GPP NAS team */
    DIAG_SUBSYS_CMAPI = 85,   /* Requested by CMAPI */
    DIAG_SUBSYS_SSM = 86,
    DIAG_SUBSYS_TDSCDMA = 87, /* Requested by TDSCDMA team */
    DIAG_SUBSYS_SSM_TEST = 88,
    DIAG_SUBSYS_MPOWER = 89, /* Requested by MPOWER team */
    DIAG_SUBSYS_QDSS = 90,   /* For QDSS STM commands */
    DIAG_SUBSYS_CXM = 91,
    DIAG_SUBSYS_GNSS_SOC = 92, /* Secondary GNSS system */
    DIAG_SUBSYS_TTLITE = 93,
    DIAG_SUBSYS_FTM_ANT = 94,
    DIAG_SUBSYS_MLOG = 95,
    DIAG_SUBSYS_LIMITSMGR = 96,
    DIAG_SUBSYS_EFSMONITOR = 97,
    DIAG_SUBSYS_DISPLAY_CALIBRATION = 98,
    DIAG_SUBSYS_VERSION_REPORT = 99,
    DIAG_SUBSYS_DS_IPA = 100,
    DIAG_SUBSYS_SYSTEM_OPERATIONS = 101,
    DIAG_SUBSYS_CNSS_POWER = 102,
    DIAG_SUBSYS_LWIP = 103,
    DIAG_SUBSYS_IMS_QVP_RTP = 104,
    DIAG_SUBSYS_STORAGE = 105,
    DIAG_SUBSYS_WCI2 = 106,
    DIAG_SUBSYS_AOSTLM_TEST = 107,
    DIAG_SUBSYS_CFCM = 108,
    DIAG_SUBSYS_CORE_SERVICES = 109,
    DIAG_SUBSYS_CVD = 110,
    DIAG_SUBSYS_MCFG = 111,
    DIAG_SUBSYS_MODEM_STRESSFW = 112,
    DIAG_SUBSYS_DS_DS3G = 113,
    DIAG_SUBSYS_TRM = 114,
    DIAG_SUBSYS_IMS = 115,
    DIAG_SUBSYS_OTA_FIREWALL = 116, /* OTA Firewall */
    DIAG_SUBSYS_I15P4 = 117,        /* IEEE 802.15.4 technologies */
    DIAG_SUBSYS_QDR = 118,          /* Qualcomm Dead Reckoning core service */

    DIAG_SUBSYS_LAST,

    /* Subsystem IDs reserved for OEM use */
    DIAG_SUBSYS_RESERVED_OEM_0 = 250,
    DIAG_SUBSYS_RESERVED_OEM_1 = 251,
    DIAG_SUBSYS_RESERVED_OEM_2 = 252,
    DIAG_SUBSYS_RESERVED_OEM_3 = 253,
    DIAG_SUBSYS_RESERVED_OEM_4 = 254,
    DIAG_SUBSYS_LEGACY = 255
} diagpkt_subsys_cmd_enum_type;

/*
 * Permitted operations.
 */
#define EFS2_DIAG_HELLO 0                           /* Parameter negotiation packet               */
#define EFS2_DIAG_QUERY 1                           /* Send information about EFS2 params         */
#define EFS2_DIAG_OPEN 2                            /* Open a file                                */
#define EFS2_DIAG_CLOSE 3                           /* Close a file                               */
#define EFS2_DIAG_READ 4                            /* Read a file                                */
#define EFS2_DIAG_WRITE 5                           /* Write a file                               */
#define EFS2_DIAG_SYMLINK 6                         /* Create a symbolic link                     */
#define EFS2_DIAG_READLINK 7                        /* Read a symbolic link                       */
#define EFS2_DIAG_UNLINK 8                          /* Remove a symbolic link or file             */
#define EFS2_DIAG_MKDIR 9                           /* Create a directory                         */
#define EFS2_DIAG_RMDIR 10                          /* Remove a directory                         */
#define EFS2_DIAG_OPENDIR 11                        /* Open a directory for reading               */
#define EFS2_DIAG_READDIR 12                        /* Read a directory                           */
#define EFS2_DIAG_CLOSEDIR 13                       /* Close an open directory                    */
#define EFS2_DIAG_RENAME 14                         /* Rename a file or directory                 */
#define EFS2_DIAG_STAT 15                           /* Obtain information about a named file      */
#define EFS2_DIAG_LSTAT 16                          /* Obtain information about a symbolic link   */
#define EFS2_DIAG_FSTAT 17                          /* Obtain information about a file descriptor */
#define EFS2_DIAG_CHMOD 18                          /* Change file permissions                    */
#define EFS2_DIAG_STATFS 19                         /* Obtain file system information             */
#define EFS2_DIAG_ACCESS 20                         /* Check a named file for accessibility       */
#define EFS2_DIAG_DEV_INFO 21                       /* Get flash device info             */
#define EFS2_DIAG_FACT_IMAGE_START 22               /* Start data output for Factory Image*/
#define EFS2_DIAG_FACT_IMAGE_READ 23                /* Get data for Factory Image         */
#define EFS2_DIAG_FACT_IMAGE_END 24                 /* End data output for Factory Image  */
#define EFS2_DIAG_PREP_FACT_IMAGE 25                /* Prepare file system for image dump */
#define EFS2_DIAG_PUT_DEPRECATED 26                 /* Write an EFS item file             */
#define EFS2_DIAG_GET_DEPRECATED 27                 /* Read an EFS item file              */
#define EFS2_DIAG_ERROR 28                          /* Semd an EFS Error Packet back through DIAG */
#define EFS2_DIAG_EXTENDED_INFO 29                  /* Get Extra information.                */
#define EFS2_DIAG_CHOWN 30                          /* Change ownership                      */
#define EFS2_DIAG_BENCHMARK_START_TEST 31           /* Start Benchmark               */
#define EFS2_DIAG_BENCHMARK_GET_RESULTS 32          /* Get Benchmark Report          */
#define EFS2_DIAG_BENCHMARK_INIT 33                 /* Init/Reset Benchmark          */
#define EFS2_DIAG_SET_RESERVATION 34                /* Set group reservation         */
#define EFS2_DIAG_SET_QUOTA 35                      /* Set group quota               */
#define EFS2_DIAG_GET_GROUP_INFO 36                 /* Retrieve Q&R values           */
#define EFS2_DIAG_DELTREE 37                        /* Delete a Directory Tree       */
#define EFS2_DIAG_PUT 38                            /* Write a EFS item file in order*/
#define EFS2_DIAG_GET 39                            /* Read a EFS item file in order */
#define EFS2_DIAG_TRUNCATE 40                       /* Truncate a file by the name   */
#define EFS2_DIAG_FTRUNCATE 41                      /* Truncate a file by a descriptor */
#define EFS2_DIAG_STATVFS_V2 42                     /* Obtains extensive file system info */
#define EFS2_DIAG_MD5SUM 43                         /* Calculate md5 hash of a file  */
#define EFS2_DIAG_HOTPLUG_FORMAT 44                 /* Format a Connected device */
#define EFS2_DIAG_SHRED 45                          /* Shred obsolete file content. */
#define EFS2_DIAG_SET_IDLE_DEV_EVT_DUR 46           /* Idle_dev_evt_dur value in mins */
#define EFS2_DIAG_HOTPLUG_DEVICE_INFO 47            /* get the hotplug device info.  */
#define EFS2_DIAG_SYNC_NO_WAIT 48                   /* non-blocking sync of remotefs device */
#define EFS2_DIAG_SYNC_GET_STATUS 49                /* query previously issued sync status */
#define EFS2_DIAG_TRUNCATE64 50                     /* Truncate a file by the name.        */
#define EFS2_DIAG_FTRUNCATE64 51                    /* Truncate a file by a descriptor.    */
#define EFS2_DIAG_LSEEK64 52                        /* Seek to requested file offset.      */
#define EFS2_DIAG_MAKE_GOLDEN_COPY 53               /* Make golden copy for Remote Storage*/
#define EFS2_DIAG_FILESYSTEM_IMAGE_OPEN 54          /*Open FileSystem Image extraction*/
#define EFS2_DIAG_FILESYSTEM_IMAGE_READ 55          /* Read File System Image.        */
#define EFS2_DIAG_FILESYSTEM_IMAGE_CLOSE 56         /* Close File System Image.      */
#define EFS2_DIAG_MAKE_GOLDEN_COPY_V2 57            /* Make golden copy */
#define EFS2_DIAG_MAKE_GOLDEN_COPY_V2_GET_STATUS 58 /* query above status */

typedef enum
{
    entry_file = 0,
    entry_directory,
    entry_link
} __attribute__((packed)) entry_type_t;

typedef struct
{
    uint32 fs_id;
    uint32 fs_type_high;
    uint32 fs_type_low;
    uint32 block_size;
    uint32 total_blocks;
    uint32 avail_blocks;
    uint32 free_blocks;
    uint32 max_file_size;
    uint32 nfiles;
    uint32 max_nfiles;
} __attribute__((packed)) fs_stat_t;

typedef struct
{
    uint32 mode;
    uint32 size;
    uint32 nlink;
    uint32 atime;
    uint32 mtime;
    uint32 ctime;
} __attribute__((packed)) stat_t;

typedef struct
{
    uint8 start; //0x75
    uint8 version;
    uint16 payload_length;
    uint8 payload[0];
} __attribute__((packed)) non_hdlc_header_t;

#define non_hdlc_end 0x7E
#define non_hdlc_start 0x7E
#define non_hdlc_version 0x01

typedef struct
{
    uint8 cmd_code;
    uint8 subsys_id;
    uint16 subsys_cmd_code;
} __attribute__((packed)) hdlr_t;
//�̶�ͷ�ṹ

//EFS2_DIAG_QUERY request/response
typedef struct
{
    hdlr_t hdlr;
} __attribute__((packed)) efs2_diag_query_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 max_name;
    uint32 max_path;
    uint32 max_link_depth;
    uint32 max_file_size;
    uint32 max_dir_entries;
    uint32 max_mounts;
} __attribute__((packed)) efs2_diag_query_resp_t;

typedef struct
{
    hdlr_t hdlr;
    //uint32 array[10];
    uint8 v;
} __attribute__((packed)) efs2_diag_hello_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 array[10];
} __attribute__((packed)) efs2_diag_hello_resp_t;

#if 0
enum s_t {
    S_IREAD = 1,
    S_IWRITE,
    S_IEXEC,
};
#endif

enum efs_diag_entry_type_t
{
    EFS2_DIAG_ENTRY_TYPE_FILE = 0,
    EFS2_DIAG_ENTRY_TYPE_DIRECTORY,
    EFS2_DIAG_ENTRY_TYPE_SYMLINK,
};

typedef struct
{
    hdlr_t hdlr;
    uint32 oflag;
    uint32 mode;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_open_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 fd;
    uint32 err;
} __attribute__((packed)) efs2_diag_open_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 fd;
} __attribute__((packed)) efs2_diag_close_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_close_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 fd;
    uint32 nbyte;
    uint32 offset;
} __attribute__((packed)) efs2_diag_read_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 fd;
    uint32 offset;
    uint32 bytes_read;
    uint32 err;
    uint8 data[0]; //Data read out
} __attribute__((packed)) efs2_diag_read_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 fd;
    uint32 offset;
    uint8 data[0];
} __attribute__((packed)) efs2_diag_write_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 fd;
    uint32 offset;
    uint32 bytes_written;
    uint32 err;
} __attribute__((packed)) efs2_diag_write_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 *oldpath;
    uint8 *newpath;
} __attribute__((packed)) efs2_diag_symlink_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_symlink_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 *path;
} __attribute__((packed)) efs2_diag_readlink_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
    uint8 *name;
} __attribute__((packed)) efs2_diag_readlink_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_unlink_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_unlink_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint16 mode;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_mkdir_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_mkdir_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_rmdir_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_rmdir_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_opendir_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 dirp;
    uint32 err;
} __attribute__((packed)) efs2_diag_opendir_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 dirp;
    uint32 seqno;
} __attribute__((packed)) efs2_diag_readdir_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 dirp;
    uint32 seqno;
    uint32 err;
    uint32 entry_type;
    uint32 mode;
    uint32 size;
    uint32 atime;
    uint32 mtime;
    uint32 ctime;
    uint8 entry_name[0];
} __attribute__((packed)) efs2_diag_readdir_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 dirp;
} __attribute__((packed)) efs2_diag_closedir_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_closedir_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 *oldpath;
    uint8 *newpath;
} __attribute__((packed)) efs2_diag_rename_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_rename_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_stat_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
    uint32 mode;
    uint32 size;
    uint32 nlink;
    uint32 atime;
    uint32 mtime;
    uint32 ctime;
} __attribute__((packed)) efs2_diag_stat_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_lstat_req_t;

typedef struct _DDD
{
    hdlr_t hdlr;
    uint32 err;
    uint32 mode;
    uint32 atime;
    uint32 mtime;
    uint32 ctime;
} __attribute__((packed)) efs2_diag_lstat_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 fd;
} __attribute__((packed)) efs2_diag_fstat_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
    uint32 mode;
    uint32 size;
    uint32 nlink;
    uint32 atime;
    uint32 mtime;
    uint32 ctime;
} __attribute__((packed)) efs2_diag_fstat_resp_t;

typedef struct
{
    //template
    hdlr_t hdlr;
    uint32 fd;
} __attribute__((packed)) efs2_diag_chmod_req_t;

typedef struct
{
    //template
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_chmod_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_statfs_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
    uint32 fs_id;
    uint32 fs_type_high;
    uint32 fs_type_low;
    uint32 block_size;
    uint32 total_blocks;
    uint32 avail_blocks;
    uint32 free_blocks;
    uint32 max_file_size;
    uint32 nfiles;
    uint32 max_nfiles;
} __attribute__((packed)) efs2_diag_statfs_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint8 perm_bits;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_access_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_access_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
    uint32 pkt[8];
} __attribute__((packed)) efs2_diag_error_resp_t;

typedef struct
{
    //template
    hdlr_t hdlr;
    uint32 fd;
} __attribute__((packed)) efs2_diag_chown_req_t;

typedef struct
{
    //template
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_chown_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint16 sequence_number;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_deltree_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint16 sequence_number;
    uint32 err;
} __attribute__((packed)) efs2_diag_deltree_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint16 sequence_number;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_syncnowait_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint32 err;
} __attribute__((packed)) efs2_diag_syncnowait_resp_t;

typedef struct
{
    hdlr_t hdlr;
    uint16 sequence_number;
    uint32 sync_token;
    uint8 path[0];
} __attribute__((packed)) efs2_diag_syncgetstatus_req_t;

typedef struct
{
    hdlr_t hdlr;
    uint16 sequence_number;
    uint8 sync_status;
    uint32 err;
} __attribute__((packed)) efs2_diag_syncgetstatus_resp_t;

typedef void (*process_cb_t)(void *notifier, int pos);
typedef int (*cb_t)(uint8 *, void *);

int init_module(int fd);
int open_file(uint8 *path, uint32 oflag, uint32 mode, uint32 *fd);
int close_file(uint32 fd);
int get_file_size(uint32 fd, size_t *file_size);
int remove_file(uint8 *path);
int remove_dir(uint8 *path);
int remove_dir_all(uint8 *path, void *);
int write_file(char *filename, uint32 fd, process_cb_t cb, void *notifer);
int read_file(uint32 fd, char *dst_filename, process_cb_t cb, void *notifier);
int retrieve_stat(char *path, stat_t *ptr);
int create_dir(uint8 *path);
int enumerate_dir(uint8 *path, cb_t cb);
int reset_device(void);
int read_dir(uint32 dirp, uint32 seqno);

int transfer_file(uint8 *path, char *fullfilename);
int fetch_file(uint8 *path, char *fullfilename);
char *get_transfer_error(int err);
int showup(uint8 *path, void *_arg);

#endif
