#include "quectel_comm.h"
#include "quectel_debug.h"

static int file_get_value(const char *fpath, int base)
{
    int value;
    FILE *fp = fopen(fpath, "r");

    if (!fp)
    {
        dbg("fopen %s failed, %s", fpath, strerror(errno));
        return 0;
    }

    if (base == 10)
        fscanf(fp, "%d", &value);

    if (base == 16)
        fscanf(fp, "%x", &value);
    fclose(fp);
    return value;
}

static int dir_get_child(const char *dirname, const char *prefix, char *child)
{
    struct dirent *ent = NULL;
    child[0] = '\0';
    DIR *dirptr = opendir(dirname);
    if (!dirptr)
    {
        dbg("cannot opendir %s", dirname);
        return -1;
    }

    while ((ent = readdir(dirptr)))
    {
        if (ent->d_name[0] == '.')
            continue;

        if (!strncasecmp(ent->d_name, prefix, strlen(prefix)))
        {
            sprintf(child, "%s", ent->d_name);
            break;
        }
    }
    closedir(dirptr);
    return 0;
}

static int ttyusb_dev_detect(char **pp_diag_port, int ifno)
{
    int ret = 1;
    struct dirent *ent = NULL;
    DIR *pDir;
    char dir[255] = "/sys/bus/usb/devices";
    pDir = opendir(dir);
    if (!pDir)
    {
        dbg("cannot opendir %s", dir);
        return -1;
    }

    while ((ent = readdir(pDir)))
    {
        char path[255];
        char dev[255];
        int idVendor;

        char diag_port[32] = "\0";
        if (ent->d_name[0] == '.')
            continue;

        snprintf(path, sizeof(path), "%s/%s/idVendor", dir, ent->d_name);
        if (access(path, F_OK))
            continue;

        idVendor = file_get_value(path, 16);

        if (idVendor != 0x05c6 && idVendor != 0x2c7c)
            continue;

        snprintf(path, sizeof(path), "%s/%s:1.%d", dir, ent->d_name, ifno);
        dir_get_child(path, "ttyUSB", diag_port);
        if (diag_port[0] == '\0')
            continue;

        snprintf(dev, sizeof(dev), "/dev/%s", diag_port);
        dbg("find tty: %s", dev);
        *pp_diag_port = strdup(dev);
    }
    closedir(pDir);

    return ret;
}

int detect_diag_port(char **diag_port)
{
    return ttyusb_dev_detect(diag_port, 0);
}
