### source user-specific settings ###
if [ -f ~/.ucfg ]; then
    . ~/.ucfg
else
    . ~/.ucfg_default
fi

# modify history size
HISTSIZE=5000
HISTFILESIZE=10000

# disable XON/XOFF flow control, enables ctrl-s forward search in bash
[[ $- == *i* ]] && stty -ixon

# nvidia jetson stuff
alias cuda_version='/usr/local/cuda/bin/nvcc --version'
alias l4t_version='cat /etc/nv_tegra_release'

# swagger editor helpers
alias swedrun='docker run -d -p 80:8080 --name swag_edit swaggerapi/swagger-editor'
alias swedstop='docker stop swag_edit && docker rm swag_edit'

### cargo/rust stuff ###
# cargo format
alias cf='cargo fmt --all'
# cargo build
alias cb='cargo build --workspace --examples --tests --benches --bins'
# cargo build edge release
# alias cber='(. /opt/edge-os/4.0.1/env* && cargo build --target aarch64-unknown-linux-gnu --release)'
# cargo clippy
alias clip='cargo clippy --workspace --examples --tests --benches'
# cargo format/build/clippy/test, everything needed to push a commit
alias call='cb && cf && clip && cargo test'

### Cross-Compilation tools ###
# alias buidl='buidl.sh'
# alias b-cscgo-hub-pc='buidl \
                      # --maker cmake \
                      # --recipe $HOME/tools/build_recipes/cscgo-hub.sh \
                      # --output $HOME/build_artifacts/pc/cscgo-hub \
                      # $HOME/repos/cscgo-hub'
# alias b-cscgo-hub-nano='buidl \
                        # --maker cmake \
                        # --recipe $HOME/tools/build_recipes/cscgo-hub.sh \
                        # --toolchain $HOME/toolchains/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu/bin \
                        # --rootfs $HOME/rootfss/jetson_nano \
                        # --output $HOME/build_artifacts/nano/cscgo-hub \
                        # $HOME/repos/cscgo-hub'

### Git Stuff ###
# Clone Cnktd Tech
alias clone_cnktd='git_helper.sh clone_cust --user "$GIT_NAME_CNKTD" --email "$GIT_EMAIL_CNKTD" --ssh-id "$HOME/.ssh/id_ed25519_cnktd" --repo'
# Clone personal
alias clone_personal='git_helper.sh clone_cust --user "$GIT_NAME_PERSONAL" --email "$GIT_EMAIL_PERSONAL" --ssh-id "$HOME/.ssh/id_ed25519" --repo'
# Clone quectel
alias clone_quectel='git_helper.sh clone_cust --user "$GIT_NAME_QUECTEL" --email "$GIT_EMAIL_QUECTEL" --ssh-id "$HOME/.ssh/id_ed25519_quectel" --repo'

### Quectel Stuff ###
alias qe='sudo $HOME/tools/QExplorer'
alias ql='qe -m list'
alias qr='qe -m reset'
alias qd='qe -m delete -r'
# alias qup='qe -m upload -r /datatx/upgrade_nand -f'
qup() {
    # cp $1 /run/user/1000/2.bin
    qe -m upload -r /datatx/upgrade_nand -f $1 # && \
        # qe -m upload -r /datatx/wiggle -f /run/user/1000/2.bin
    # rm /run/user/1000/2.bin
}

### CNKTD Stuff ###
# helpers for finding the correct usb-serial device
find_ftdi() {
    if sudo [ -f '/proc/tty/driver/usbserial' ]; then
        PNUM=$(sudo cat /proc/tty/driver/usbserial | grep FTDI | sed -E 's/([0-9]+):.*/\1/g')
    fi
    if [ ! -z "$PNUM" ]; then
        echo "/dev/ttyUSB${PNUM}"
    fi
}
find_dm() {
    if sudo [ -f '/proc/tty/driver/usbserial' ]; then
        PNUM=$(sudo cat /proc/tty/driver/usbserial | grep "GSM modem" | head -1 | sed -E 's/([0-9]+):.*/\1/g')
    fi
    if [ ! -z "$PNUM" ]; then
        echo "/dev/ttyUSB${PNUM}"
    fi
}
find_at() {
    if sudo [ -f '/proc/tty/driver/usbserial' ]; then
        PNUM=$(sudo cat /proc/tty/driver/usbserial | grep "GSM modem" | tail -1 | sed -E 's/([0-9]+):.*/\1/g')
    fi
    if [ ! -z "$PNUM" ]; then
        echo "/dev/ttyUSB${PNUM}"
    fi
}

find_usb_serial() {
    udevadm info -e \
        --property-match=ID_USB_VENDOR_ID=$1 \
        --property-match=ID_USB_MODEL_ID=$2 | \
        grep DEVNAME=/dev/tty | \
        sed -E 's/.+=(.+)/\1/g'
}

# helpers for sending console commands and AT commands
scmd() {
    DEV_PORT=$(find_ftdi)
    if [ -z $DEV_PORT ]; then
        echo "No FTDI usb serial device found"
        return
    fi
    # replace ! with unicode char for !, or bash will get mad
    STRING=$(echo $1 | sed -E 's/!/\x21/g')
    QUIET=$2
    if [ -z $QUIET ]; then
        echo "sending \"$STRING\" ===> $DEV_PORT"
    fi
    stty -F $DEV_PORT 115200 && printf "$STRING" > $DEV_PORT
}
atcmd() {
    DEV_PORT=$(find_at)
    if [ -z $DEV_PORT ]; then
        echo "no quectel AT command port found"
        return
    fi
    # replace ! with unicode char for !, or bash will get mad
    STRING=$(echo $1 | sed -E 's/!/\x21/g')
    QUIET=$2
    if [ -z $QUIET ]; then
        echo "sending \"$STRING\" ===> $DEV_PORT"
    fi
    printf "$STRING" > $DEV_PORT
}

# device console commands
alias cmd_purge='scmd "\r\n" -q'
alias cmd_rst='cmd_purge && scmd "!user func rst\r\n" -q'

# AT commands
alias atcmd_dbgoff='atcmd "AT+QCFG=\"dbgctl\",0\r\n" -q'

# product specific build/load helpers
alias bceres='pushd $WIGGLE_REPO_PATH; \
              rm -rf ./build/ceres_main/; \
              make -f ./ewp/ceres.mk -j; popd'
alias uceres='qup $WIGGLE_REPO_PATH/build/ceres_main/cust_app_update.bin && cmd_rst'
alias aceres='bceres && uceres'
alias fuceres='qup $WIGGLE_REPO_PATH/build/ceres_main/cust_app_update.bin && qr'
alias faceres='bceres && fuceres'

alias bpluto='pushd $WIGGLE_REPO_PATH; \
              rm -rf ./build/pluto_main/; \
              make -f ./ewp/pluto.mk -j; popd'
alias upluto='qup $WIGGLE_REPO_PATH/build/pluto_main/cust_app_update.bin && cmd_rst'
alias apluto='bpluto && upluto'
alias fupluto='qup $WIGGLE_REPO_PATH/build/pluto_main/cust_app_update.bin && qr'
alias fapluto='bpluto && fupluto'

alias beris='pushd $WIGGLE_REPO_PATH; \
              rm -rf ./build/eris_main/; \
              make -f ./ewp/eris.mk -j; popd'
alias ueris='qup $WIGGLE_REPO_PATH/build/eris_main/cust_app_update.bin && cmd_rst'
alias aeris='beris && ueris'
alias fueris='qup $WIGGLE_REPO_PATH/build/eris_main/cust_app_update.bin && qr'
alias faeris='beris && fueris'

alias bhaumea='pushd $WIGGLE_REPO_PATH; \
              rm -rf ./build/haumea_main/; \
              make -f ./ewp/haumea.mk -j; popd'
alias uhaumea='qup $WIGGLE_REPO_PATH/build/haumea_main/cust_app_update.bin && cmd_rst'
alias ahaumea='bhaumea && uhaumea'
alias fuhaumea='qup $WIGGLE_REPO_PATH/build/haumea_main/cust_app_update.bin && qr'
alias fahaumea='bhaumea && fuhaumea'

# taking qxdm logs
alias qlog='QLog -p $(find_dm) -s $HOME/qxdm_logs'

alias scrt='screen -dmS scrt SecureCRT'



# load again for overrides
if [ -f ~/.ucfg ]; then
    . ~/.ucfg
fi
