#include "quectel_efs.h"
#include "quectel_debug.h"
#include "quectel_serial.h"

static uint8 m_req_buff[1024 * 10];
#define nonhdlc_max_size 512

static uint8 rx_buff[1024 * 10];
static uint8 tx_buff[1024 * 10];

static int m_errno;

static int build_hello_request(uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_hello_t *hellp_ptr = (efs2_diag_hello_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_HELLO;
    hellp_ptr->v = 0xFF;
    ptr->payload_length = sizeof(efs2_diag_hello_t);

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_query_request(uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_query_req_t *hellp_ptr = (efs2_diag_query_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_QUERY;
    ptr->payload_length = sizeof(efs2_diag_query_req_t);

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_open_request(uint8 *path, uint32 oflag, uint32 mode, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_open_req_t *hellp_ptr = (efs2_diag_open_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_OPEN;

    hellp_ptr->oflag = oflag;
    hellp_ptr->mode = mode;
    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_open_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_opendir_request(uint8 *path, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_opendir_req_t *hellp_ptr = (efs2_diag_opendir_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_OPENDIR;

    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_opendir_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}

static int build_close_request(uint32 fd, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_close_req_t *hellp_ptr = (efs2_diag_close_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_CLOSE;

    hellp_ptr->fd = fd;
    ptr->payload_length = sizeof(efs2_diag_close_req_t);

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}

static int build_write_request(uint32 fd, uint8 *data, uint32 len, uint32 offset, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_write_req_t *hellp_ptr = (efs2_diag_write_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_WRITE;

    hellp_ptr->fd = fd;
    hellp_ptr->offset = offset;
    memcpy(hellp_ptr->data, data, len);

    ptr->payload_length = sizeof(efs2_diag_write_req_t) + len;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}

static int build_read_request(uint32 fd, uint32 nbyte, uint32 offset, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_read_req_t *hellp_ptr = (efs2_diag_read_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_READ;

    hellp_ptr->fd = fd;
    hellp_ptr->nbyte = nbyte;
    hellp_ptr->offset = offset;
    ptr->payload_length = sizeof(efs2_diag_read_req_t);

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_mkdir_request(uint8 *path, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_mkdir_req_t *hellp_ptr = (efs2_diag_mkdir_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_MKDIR;

    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_mkdir_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_rmdir_request(uint8 *path, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_rmdir_req_t *hellp_ptr = (efs2_diag_rmdir_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_RMDIR;

    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_rmdir_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_readdir_request(uint32 dirp, uint32 seqno, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_readdir_req_t *hellp_ptr = (efs2_diag_readdir_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_READDIR;

    hellp_ptr->dirp = dirp;
    hellp_ptr->seqno = seqno;
    ptr->payload_length = sizeof(efs2_diag_readdir_req_t);

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_closedir_request(uint32 dirp, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_closedir_req_t *hellp_ptr = (efs2_diag_closedir_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_CLOSEDIR;

    hellp_ptr->dirp = dirp;
    ptr->payload_length = sizeof(efs2_diag_closedir_req_t);

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_unlink_request(uint8 *path, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_unlink_req_t *hellp_ptr = (efs2_diag_unlink_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_UNLINK;

    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_unlink_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_stat_request(uint8 *path, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_stat_req_t *hellp_ptr = (efs2_diag_stat_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_STAT;

    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_stat_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_statfs_request(uint8 *path, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_statfs_req_t *hellp_ptr = (efs2_diag_statfs_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_STATFS;

    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_statfs_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_lstat_request(uint8 *path, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_lstat_req_t *hellp_ptr = (efs2_diag_lstat_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_LSTAT;

    memcpy(hellp_ptr->path, path, strlen((char *)path));
    ptr->payload_length = sizeof(efs2_diag_lstat_req_t) + strlen((char *)hellp_ptr->path) + 1;

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}
static int build_fstat_request(uint32 fd, uint8 *out, uint16 *length)
{
    memset(m_req_buff, 0, nonhdlc_max_size);
    non_hdlc_header_t *ptr = (non_hdlc_header_t *)m_req_buff;
    ptr->start = non_hdlc_start;
    ptr->version = non_hdlc_version;

    efs2_diag_fstat_req_t *hellp_ptr = (efs2_diag_fstat_req_t *)&(ptr->payload);
    hellp_ptr->hdlr.cmd_code = DIAG_SUBSYS_CMD_F;
    hellp_ptr->hdlr.subsys_id = DIAG_SUBSYS_FS_ALTERNATE;
    hellp_ptr->hdlr.subsys_cmd_code = EFS2_DIAG_FSTAT;

    hellp_ptr->fd = fd;
    ptr->payload_length = sizeof(efs2_diag_fstat_req_t);

    *((uint8 *)m_req_buff + ptr->payload_length + 4) = non_hdlc_end;
    *length = 5 + ptr->payload_length; //include at last 7e
    memcpy(out, m_req_buff, *length);
    return 0;
}

int init_module(int fd /*not use*/)
{
    (void)fd;
    uint8 req1[] = {0x0c, 0x14, 0x3a, 0x7e};
    // uint8 resp1[] = {0x13, 0x0c, 0xd2, 0x7a, 0x7e};

    uint8 req2[] = {0x4b, 0x04, 0x0e, 0x00, 0x0d, 0xd3, 0x7e};
    // uint8 resp2[] = {0x13, 0x4b, 0x04, 0x0e, 0x00, 0x28, 0x49, 0x7e};

    uint8 req3[] = {0x4b, 0x08, 0x02, 0x00, 0x0e, 0xdf, 0x7e};
    // uint8 resp3[] = {0x4b, 0x08, 0x02, 0x00, 0x00, 0xd9, 0x19, 0x7e};

    uint8 req4[] = {0x4b, 0x12, 0x18, 0x02, 0x01, 0x00, 0xd2, 0x7e};
    // uint8 resp4[] = {0x4b, 0x12, 0x18, 0x02, 0x01, 0x00, 0xaa, 0xf0, 0x7e};

    uint8 buff[50] = {0};
    size_t bytes_read = 0;
    do
    {
        if (tx_data(req1, sizeof(req1)) != 0)
            goto __error__;
        if (rx_data(buff, 50, &bytes_read) != 0)
            goto __error__;
        //if(memcmp(buff, resp1, sizeof(resp1)) != 0) goto __error__;

        if (tx_data(req2, sizeof(req2)) != 0)
            goto __error__;
        if (rx_data(buff, 50, &bytes_read) != 0)
            goto __error__;
        //if(memcmp(buff, resp2, sizeof(resp2)) != 0) goto __error__;

        if (tx_data(req3, sizeof(req3)) != 0)
            goto __error__;
        if (rx_data(buff, 50, &bytes_read) != 0)
            goto __error__;
        //if(memcmp(buff, resp3, 4) != 0) goto __error__;

        if (tx_data(req4, sizeof(req4)) != 0)
            goto __error__;
        if (rx_data(buff, 50, &bytes_read) != 0)
            goto __error__;
        //if(memcmp(buff, resp4, sizeof(resp4)) != 0) goto __error__;

    } while (0);

    return 0;
__error__:
    return -1;
}

int open_file(uint8 *path, uint32 oflag, uint32 mode, uint32 *fd)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    build_open_request(path, oflag, mode, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(rx_buff, tx_buff, 2) == 0)
    {
        efs2_diag_open_resp_t *ptr = (efs2_diag_open_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            *fd = ptr->fd;
            return 0;
        }
    }
    return -1;
}

int close_file(uint32 fd)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    uint16 bytes_to_read = 1024 * 2;
    //efs_request_builder* builder = new efs_nonhdlc_request();
    build_close_request(fd, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, bytes_to_read, &bytes_read);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_close_resp_t *ptr = (efs2_diag_close_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            return 0;
        }
    }
    return -1;
}
int retrieve_stat(char *path, stat_t *ptr_info)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    build_stat_request((uint8 *)path, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(rx_buff, tx_buff, 2) == 0)
    {
        efs2_diag_stat_resp_t *ptr = (efs2_diag_stat_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            ptr_info->mode = ptr->mode;
            ptr_info->size = ptr->size;
            ptr_info->nlink = ptr->nlink;
            ptr_info->atime = ptr->atime;
            ptr_info->mtime = ptr->mtime;
            ptr_info->ctime = ptr->ctime;
            return 0;
        }
    }
    return -1;
}
int retrieve_statfs(char *path, fs_stat_t *path_state_ptr)
{
    size_t bytes_read;
    uint16 bytes_to_send;

    build_statfs_request((uint8 *)path, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(rx_buff, tx_buff, 2) == 0)
    {
        efs2_diag_statfs_resp_t *ptr = (efs2_diag_statfs_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            path_state_ptr->fs_id = ptr->fs_id;
            path_state_ptr->fs_type_high = ptr->fs_type_high;
            path_state_ptr->fs_type_low = ptr->fs_type_low;
            path_state_ptr->block_size = ptr->block_size;
            path_state_ptr->total_blocks = ptr->total_blocks;
            path_state_ptr->avail_blocks = ptr->avail_blocks;
            path_state_ptr->free_blocks = ptr->free_blocks;
            path_state_ptr->max_file_size = ptr->max_file_size;
            path_state_ptr->nfiles = ptr->nfiles;
            path_state_ptr->max_nfiles = ptr->max_nfiles;
            return 0;
        }
    }
    return -1;
}

void get_fs_info(fs_stat_t *ptr, uint64 *total, uint64 *avail, uint64 *free1)
{
    uint32 blocksize = ptr->block_size;
    *total = blocksize * ptr->total_blocks;
    *avail = blocksize * ptr->avail_blocks;
    *free1 = blocksize * ptr->free_blocks;
}
int get_file_size(uint32 fd, size_t *file_size)
{
    size_t bytes_read;
    uint16 bytes_to_send;

    build_fstat_request(fd, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(rx_buff, tx_buff, 2) == 0)
    {
        efs2_diag_fstat_resp_t *ptr = (efs2_diag_fstat_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            *file_size = ptr->size;
            return 0;
        }
    }
    return -1;
}
int read_inner(uint32 fd, uint8 *bytes, uint16 bytes_want_read, uint16 *bytes_read, uint32 offset)
{
    uint32 index = 0;
    int ret = -1;
    uint16 want_to_write = 0;
    size_t bytes_read_temp = 0;

    build_read_request(fd, bytes_want_read, offset, tx_buff, &want_to_write);
    tx_data(tx_buff, want_to_write);
    do
    {
        ret = rx_data(&rx_buff[index], 1024, &bytes_read_temp);
        if (ret != 0)
            break;
        index += bytes_read_temp;
        if (bytes_read_temp == 0 || 1024 != bytes_read_temp)
            break;
    } while (1);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_read_resp_t *ptr = (efs2_diag_read_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            *bytes_read = ptr->bytes_read;
            memcpy(bytes, &ptr->data[0], ptr->bytes_read);
            return 0;
        }
    }

    return -1;
}

int write_inner(uint32 fd, uint8 *bytes, uint16 bytes_to_write, uint16 *bytes_written, uint32 offset)
{
    uint16 want_to_write = 0;
    size_t bytes_read = 0;
    build_write_request(fd, bytes, bytes_to_write, offset, tx_buff, &want_to_write);
    tx_data(tx_buff, want_to_write);
    rx_data(rx_buff, 1024 * 4, &bytes_read);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_write_resp_t *ptr = (efs2_diag_write_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            *bytes_written = ptr->bytes_written;
            return 0;
        }
    }
    return -1;
}

int remove_file(uint8 *path)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    uint16 bytes_to_read = 1024 * 2;
    //efs_request_builder* builder = new efs_nonhdlc_request();
    build_unlink_request(path, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, bytes_to_read, &bytes_read);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_unlink_resp_t *ptr = (efs2_diag_unlink_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            return 0;
        }
    }
    return -1;
}
int read_file(uint32 fd, char *dst_filename, process_cb_t cb, void *notifier)
{
    FILE *fp;
    uint32 offset = 0;
    uint16 bytes_read = 0;
    int ret = 1;
    uint8 buff[1024];

    fp = fopen(dst_filename, "w");
    if (fp == NULL)
    {
        return -1;
    }

    do
    {
        ret = read_inner(fd, buff, 1024, &bytes_read, offset);
        if (ret != 0)
            return -1;
        bytes_read = fwrite(buff, 1, bytes_read, fp);
        offset += bytes_read;
        if (cb)
            cb(notifier, offset);
        if (1024 != bytes_read)
            break;
    } while (1);

    fclose(fp);
    return 0;
}
int GetFileSize(FILE *fp, uint32 *filesize)
{
    fseek(fp, 0, SEEK_END);
    if (filesize != NULL)
    {
        *filesize = ftell(fp);
    }
    fseek(fp, 0, SEEK_SET);
    return 0;
}

int getfilesize(char *filename)
{
    FILE *fp;
    int len;
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        dbg("open %s failed, %s", filename, strerror(errno));
        return 0;
    }
    fseek(fp, 0, SEEK_END);
    len = ftell(fp);
    fclose(fp);
    return len;
}

int write_file(char *filename, uint32 fd, process_cb_t cb, void *notifer)
{
    uint8 temp[1024 + 1];
    FILE *fp;
    uint32 numberofBytesRead;
    uint32 filesize = 0;
    uint32 offset = 0;
    uint16 bytes_written = 0;

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        dbg("open %s failed, %s", filename, strerror(errno));
        return -1;
    }

    GetFileSize(fp, &filesize);
    while ((numberofBytesRead = fread(temp, sizeof(uint8), 1024, fp)))
    {
        if (0 == numberofBytesRead)
            break; //end of file
        if (write_inner(fd, temp, numberofBytesRead, &bytes_written, offset) == 0)
        {
            offset += numberofBytesRead;
            if (cb)
                cb(notifer, offset);
        }
        else
            goto __exit__;
    }

    return 0;
__exit__:
    fclose(fp);

    return -1;
}
/*
transfer file to path
return
1: 	path not exist
2:	inner error
3: 	unknown error
4: 	space not enough
5:	write error
0:	ok
*/
char *get_file_name(char *fullfilename)
{
    char *p = strrchr(fullfilename, '/');
    return p == NULL ? NULL : ++p;
}
char *get_transfer_error(int err)
{
    switch (err)
    {
    case 1:
    {
        return "access file error";
    }
    break;
    case 2:
    {
        return "get file sizeerror";
    }
    break;
    case 3:
    {
        return "get space error";
    }
    break;
    case 4:
    {
        return "unknown";
    }
    break;
    case 5:
    {
        return "space is not enough";
    }
    break;
    case 6:
    {
        return "create file error";
    }
    break;
    case 7:
    {
        return "write file error";
    }
    break;
    case 8:
    {
        return "close";
    }
    break;
    }
    return "";
}

int fetch_file(uint8 *path, char *fullfilename)
{
    uint32 fd;
    fs_stat_t path_state = {0};
    uint32 oflag = 0;
    uint32 mode = 0;

    if (retrieve_statfs((char *)path, &path_state) != 0)
    {
        dbg("retrieve_statfs path %s error", path);
        return 3;
    }

    dbg("start retrive file %s", path);
    oflag = O_NONBLOCK | O_RDONLY;
    mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
    if (open_file(path, oflag, mode, &fd) != 0)
    {
        return 6;
    }
    if (read_file(fd, fullfilename, NULL, NULL) != 0)
    {
        close_file(fd);
        return 7;
    }
    if (close_file(fd) != 0)
    {
        return 8;
    }
    dbg("%s transfer %s to %s ok", __func__, path, fullfilename);
    return 0;
}

int transfer_file(uint8 *path, char *fullfilename)
{
    uint32 fd;
    uint32 filesize;
    char *filename;
    fs_stat_t path_state = {0};
    char dst_filename[256];
    uint32 oflag = 0;
    uint32 mode = 0;

    if (access(fullfilename, F_OK) != 0)
        return 1;

    filesize = getfilesize(fullfilename);
    if (filesize == 0)
        return 2;

    if (retrieve_statfs((char *)path, &path_state) != 0)
        return 3;

    if (filesize > path_state.avail_blocks * path_state.block_size)
    {
        dbg("space not enough for new file.");
        return 5;
    }

    filename = get_file_name(fullfilename);
    if (filename == NULL)
        filename = fullfilename;
    sprintf(dst_filename, "%s/%s", path, filename);

    dbg("start transfer %s to %s", filename, path);
    oflag = O_TRUNC | O_CREAT | O_WRONLY;
    mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
    if (open_file((uint8 *)dst_filename, oflag, mode, &fd) != 0)
    {
        return 6;
    }
    if (write_file(fullfilename, fd, NULL, NULL) != 0)
    {
        close_file(fd);
        return 7;
    }
    if (close_file(fd) != 0)
    {
        return 8;
    }
    dbg("%s transfer %s to %s ok", __func__, filename, path);
    return 0;
}

int reset_device(void)
{
    size_t bytes_read;
    const uint8_t cmd1[8] = { 0x4b, 0x12, 0x18, 0x02, 0x01, 0x00, 0xd2, 0x7e };
    const uint8_t cmd2[9] = { 0x7e, 0x01, 0x04, 0x00, 0x4b, 0x25, 0x03, 0x00, 0x7e };
    memcpy(tx_buff, cmd1, sizeof(cmd1));
    tx_data(tx_buff, sizeof(cmd1));
    rx_data(rx_buff, 512, &bytes_read);
    memcpy(tx_buff, cmd2, sizeof(cmd2));
    tx_data(tx_buff, sizeof(cmd2));
    rx_data(rx_buff, 512, &bytes_read);
    return 0;
}

int enumerate_dir(uint8 *path, cb_t cb)
{
    int seqno = 1;
    size_t bytes_read;
    uint16 bytes_to_send;
    uint16 length = 0;
    uint8 temp[256] = {0};
    efs2_diag_opendir_resp_t *ptr1 = 0;
    uint32 dirp = 0;
    build_opendir_request((uint8 *)path, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(rx_buff, tx_buff, 2) == 0)
    {
        ptr1 = (efs2_diag_opendir_resp_t *)&rx_buff[4];
        m_errno = ptr1->err;
        if (ptr1->err != 0)
        {
            return -1;
        }
    }

    dirp = ptr1->dirp;
    do
    {
        build_readdir_request(dirp, seqno++, tx_buff, &bytes_to_send);
        tx_data(tx_buff, bytes_to_send);
        rx_data(rx_buff, 512, &bytes_read);
        if (memcmp(rx_buff, tx_buff, 2) == 0)
        {
            uint8 _resp[1024] = {0};
            efs2_diag_readdir_resp_t *resp = (void *)_resp;

            memcpy(&_resp, (void *)(rx_buff + 4), 1024);
            if (*resp->entry_name == 0x00)
            {
                break;
            }
            else
            {
                sprintf((char *)temp, "%s/%s", path, resp->entry_name);
                if (resp->entry_type == EFS2_DIAG_ENTRY_TYPE_DIRECTORY)
                {
                    enumerate_dir(temp, cb);
                }
                cb(temp, resp);
            }
        }
        else
        {
            break;
        }
    } while (1);

    build_closedir_request(dirp, tx_buff, &length);
    tx_data(tx_buff, length);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_closedir_resp_t *ptr2 = (efs2_diag_closedir_resp_t *)&rx_buff[4];

        m_errno = ptr2->err;
        if (m_errno == 0)
            return 0;
    }
    return -1;
}
int create_dir(uint8 *path)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    uint16 bytes_to_read = 1024 * 2;
    //efs_request_builder* builder = new efs_nonhdlc_request();
    build_mkdir_request(path, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, bytes_to_read, &bytes_read);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_mkdir_resp_t *ptr = (efs2_diag_mkdir_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            return 0;
        }
    }
    return -1;
}
int open_dir(uint8 *path, uint32 *pdir)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    //efs_request_builder* builder = new efs_nonhdlc_request();
    build_opendir_request((uint8 *)path, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(rx_buff, tx_buff, 2) == 0)
    {
        efs2_diag_opendir_resp_t *ptr = (efs2_diag_opendir_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            *pdir = ptr->dirp;
            return 0;
        }
    }
    return -1;
}
int close_dir(uint32 dirp)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    //efs_request_builder* builder = new efs_nonhdlc_request();
    build_closedir_request(dirp, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, 512, &bytes_read);
    if (memcmp(rx_buff, tx_buff, 2) == 0)
    {
        efs2_diag_closedir_resp_t *ptr = (efs2_diag_closedir_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            return 0;
        }
    }
    return -1;
}
int remove_dir(uint8 *path)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    uint16 bytes_to_read = 1024 * 2;
    //efs_request_builder* builder = new efs_nonhdlc_request();
    build_rmdir_request(path, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, bytes_to_read, &bytes_read);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_rmdir_resp_t *ptr = (efs2_diag_rmdir_resp_t *)&rx_buff[4];
        m_errno = ptr->err;
        if (ptr->err == 0)
        {
            return 0;
        }
    }
    return -1;
}

int remove_dir_all(uint8 *path, void *_arg)
{
    efs2_diag_readdir_resp_t *arg = _arg;
    switch (arg->entry_type)
    {
    case EFS2_DIAG_ENTRY_TYPE_FILE:
    case EFS2_DIAG_ENTRY_TYPE_SYMLINK:
        return remove_file(path);
    case EFS2_DIAG_ENTRY_TYPE_DIRECTORY:
        return remove_dir(path);
    default:
        dbg("invalid type for %s", path);
        break;
    }
    return 0;
}

int read_dir(uint32 dirp, uint32 seqno)
{
    size_t bytes_read;
    uint16 bytes_to_send;
    uint16 bytes_to_read = 1024 * 2;

    //efs_request_builder* builder = new efs_nonhdlc_request();
    build_readdir_request(dirp, seqno++, tx_buff, &bytes_to_send);
    tx_data(tx_buff, bytes_to_send);
    rx_data(rx_buff, bytes_to_read, &bytes_read);
    if (memcmp(tx_buff, rx_buff, 2) == 0)
    {
        efs2_diag_readdir_resp_t *resp = (efs2_diag_readdir_resp_t *)&rx_buff[4];
        if (*resp->entry_name == 0x00)
        {
            return -1;
        }
        else
        {
            //item_ptr->path = std::string((char*)resp->entry_name);
            if (resp->entry_type == EFS2_DIAG_ENTRY_TYPE_FILE)
            {
                //item_ptr->type = entry_file;
                dbg("file name = %s", resp->entry_name);
            }
            else if (resp->entry_type == EFS2_DIAG_ENTRY_TYPE_DIRECTORY)
            {
                //item_ptr->type = entry_directory;
                dbg("directory name = %s", resp->entry_name);
            }
            else
            {
                dbg("link name = %s", resp->entry_name);
                //item_ptr->type = entry_link;
            }
            return 0;
        }
    }
    return -2;
}

int showup(uint8 *path, void *_arg)
{
    static int show_banner = 0;
    efs2_diag_readdir_resp_t *arg = _arg;

    if (!show_banner)
    {
        dbg("\t%-10s %10s     \t%s", "Type", "Size", "Path");
        show_banner = 1;
    }

    dbg("\t%-10s %10d Byte\t%s",
        (arg->entry_type == EFS2_DIAG_ENTRY_TYPE_FILE
             ? "File"
             : (arg->entry_type == EFS2_DIAG_ENTRY_TYPE_DIRECTORY
                    ? "Directory"
                    : (arg->entry_type == EFS2_DIAG_ENTRY_TYPE_SYMLINK ? "Link"
                                                                       : "UNKNOW"))),
        arg->size, path);
    return 0;
}
