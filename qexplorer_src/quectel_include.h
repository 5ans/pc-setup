#ifndef QUECTEL_INCLUDE_H
#define QUECTEL_INCLUDE_H

#include <stdio.h>

//linux platform include start
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <linux/errno.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdarg.h>
#include <pwd.h>
#include <grp.h>
#include <stdint.h>
#include <sys/time.h>
#include <pthread.h>
#include <stdlib.h>
#include <stddef.h>
//linux platform include end

typedef unsigned char byte;

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

#endif