Set-ExecutionPolicy Bypass -Scope Process -Force
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
refreshenv
choco upgrade python3 -y
choco upgrade vim-tux -y
choco upgrade ctags -y
refreshenv
choco upgrade fd -y
choco upgrade ripgrep -y
choco upgrade ack -y
choco upgrade git -y
choco upgrade curl -y
refreshenv
git clone https://github.com/VundleVim/Vundle.vim.git $env:USERPROFILE\vimfiles\bundle\Vundle.vim
Copy-Item _vimrc $env:USERPROFILE
Copy-Item syntax $env:USERPROFILE\vimfiles\syntax -Recurse -Force
Copy-Item colors $env:USERPROFILE\vimfiles\colors -Recurse -Force
$vimver = (vim --version | Select-String -Pattern ("Vi IMproved")) -replace '^.*IMproved | \(.*$|\.'
Copy-Item fixgvimborder.dll "C:\Program Files\Vim\vim$vimver" -Force
Copy-Item loadfixgvimborder.dll "C:\Program Files\Vim\vim$vimver" -Force
If(Test-Path C:\Temp) { "Found C:\Temp" } Else { New-Item -ItemType Directory -Path C:\Temp }
vim -c PluginInstall -c qall!
Copy-Item bundle $env:USERPROFILE\vimfiles -Recurse -Force
cd $env:USERPROFILE\vimfiles\bundle\powerline-fonts
.\install.ps1

