Set-ExecutionPolicy Bypass -Scope Process -Force
Set-Variable -Name "MainInstallDir" -Value (Read-Host -Prompt "Install location")
New-Item $MainInstallDir -Type Directory -Force
$env:ChocolateyInstall = Join-Path $MainInstallDir 'chocolatey'
[System.Environment]::SetEnvironmentVariable("ChocolateyInstall", $env:ChocolateyInstall, "Machine")
[Environment]::SetEnvironmentVariable("ChocolateyInstall", $env:ChocolateyInstall, "User")
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
refreshenv
choco install python2 -y

##### VIM installation #####
#choco install vim-tux -y
$vimmainpath = Join-Path $MainInstallDir 'VIM\vim81'
New-Item $vimmainpath -Type Directory -Force
$archivepath = Join-Path $vimmainpath 'archive64.exe'
$vimtuxuri = "https://tuxproject.de/projects/vim/complete-x64.exe"
Invoke-WebRequest -Uri $vimtuxuri -OutFile $archivepath
$args = @("-y")
Start-Process -Filepath $archivepath -ArgumentList $args
$installerpath = Join-Path $vimmainpath 'install.exe' 
$args = @("-install-icons","-add-start-menu")
Start-Sleep -s 5
Start-Process -Filepath $installerpath

choco install ctags -y
choco install cygwin -y
refreshenv

C:\tools\cygwin\cygwinsetup -a x86_64 -f -s 'http://cygwin.mirror.constant.com' -q -P 'cscope' --prune-install
choco install fd -y
choco install ripgrep -y
choco install ack -y
choco install git -y
choco install curl -y
refreshenv

##### additional VIM installation #####
$vimruntimepath = Join-Path $MainInstallDir 'VIM\runtime'
New-Item $vimruntimepath -Type Directory -Force
$vimfilespath = Join-Path $vimruntimepath 'vimfiles'
New-Item $vimfilespath -Type Directory -Force
Copy-Item _vimrc_redirect $env:USERPROFILE
If(Test-Path (Join-Path $env:USERPROFILE '_vimrc')) {
    Remove-Item (Join-Path $env:USERPROFILE '_vimrc')
}
Rename-Item -Path (Join-Path $env:USERPROFILE '_vimrc_redirect') -NewName "_vimrc"
Copy-Item _vimrc_rd $vimfilespath
If(Test-Path (Join-Path $vimfilespath '_vimrc')) {
    Remove-Item (Join-Path $vimfilespath '_vimrc')
}
Rename-Item -Path (Join-Path $vimfilespath '_vimrc_rd') -NewName "_vimrc"

git clone https://github.com/VundleVim/Vundle.vim.git $vimfilespath\bundle\Vundle.vim
Copy-Item syntax $vimfilespath\syntax -Recurse -Force
Copy-Item colors $vimfilespath\colors -Recurse -Force
Copy-Item fixgvimborder.dll $vimmainpath -Force
Copy-Item loadfixgvimborder.dll $vimmainpath -Force
If(Test-Path C:\Temp) { "Found C:\Temp" } Else { New-Item -ItemType Directory -Path C:\Temp }
vim -c PluginInstall -c qall!
Copy-Item bundle $vimfilespath -Recurse -Force
cd $vimfilespath\bundle\powerline-fonts
.\install.ps1
