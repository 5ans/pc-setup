# install Ubuntu 24.04L
    main partition: ext4, remaining size, / primary

# Unfuck terminal on sysytem76
    open a terminal by:
    - alt+f2
    - gnome-terminal -e bash
    - sudo usermod -s /bin/bash $USER
    - exit

# upgrade sources.list
    open /etc/apt/sources.list.d/ubuntu.sources and change The first line after the large block comment from
    `Types: deb`
    to
    `Types: deb deb-src`

# upgrade os
    sudo apt update -y
    sudo apt dist-upgrade -y
    reboot
    sudo apt autoremove -y

# install git
    sudo apt install -y git

# clone this repo
    git clone https://gitlab.com/5ans/pc-setup.git
    cd ./pc-setup

# copy the default user config, and populate
    cp ./setup/.ucfg_default ~/.ucfg
    vi ~/.ucfg

# execute the setup script (Do not run as sudo)
    ./main-setup.sh

# gnome terminal changes
    # change font to Ubuntu Mono derivative Powerline 12
    # change schemes to solarized dark
    # set transparency to 5-10%
    # change F11 shortcut to ALT+ENTER
    # disable menu accelerator in "General" tab

# configure git
    git config --global user.name "<name>"
    git config --global user.email "<email>"

# generate ssh key
    ssh-keygen -t ed25519
    # set password for key when creating, add to git registry (gitlab/github), revoke unused keys

# displaylink driver for wavlink dock (optional)
    # if driver is already installed, and you need to upgrade, or did a kernel upgrade
        sudo displaylink-installer uninstall
        sudo reboot
    # install the new driver
        curl -o $HOME/Downloads/synaptics-repository-keyring.deb https://www.synaptics.com/sites/default/files/Ubuntu/pool/stable/main/all/synaptics-repository-keyring.deb
        sudo apt install $HOME/Downloads/synaptics-repository-keyring.deb
        sudo apt update
        sudo apt install displaylink-driver
        sudo reboot

# install software
    # brave (fuck chrome)
        # easy
    # slack (optional)
        # install via ubuntu software
        # sign into relevant workspaces
        # set dark theme
    # zoom (optional)
        curl --remote-name https://zoom.us/client/latest/zoom_amd64.deb
        sudo apt install ./zoom_amd64.deb -y
        sign in with bright google account
    # keybase (optional)
        curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
        sudo apt install ./keybase_amd64.deb -y
        set darkmode
    # rust/cargo
        curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
        cargo install cargo-xwin
        rustup target add x86_64-unknown-linux-musl
        rustup target add x86_64-pc-windows-msvc
    # cbor-diag command line tool
        cargo install cbor-diag-cli
    # SecureCRT
        https://www.vandyke.com/cgi-bin/releases.php?product=securecrt
        **NOTE**: you must get 9.3 or older for the CNKTD license
        **NOTE**: on Ubuntu 24, you will need to copy some of the libs in libicu70\_70.1-2\_amd64 into /usr/lib/x86_64-linux-gnu and create symlinks for version 70 pointing to these objects.
    # Microsoft Teams
        "Install" it as a browser app
