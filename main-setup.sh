#!/bin/bash
#
install_if_needed() {
    INSTALLED=$(apt list --installed $1 2>/dev/null | grep installed)
    if [ -z "$INSTALLED" ]; then
        echo "============================================="
        echo "     installing $1"
        echo "============================================="
        sudo apt install $1 -y
    fi
}

# install build tools
install_if_needed build-essential
install_if_needed libtinfo5

# install python2
install_if_needed python2

# install curl
install_if_needed curl

# install zlib compression tool
install_if_needed qpdf

# support for building musl abi based rust applications
install_if_needed musl-tools

# install vim
if [ -z $(which vim) ]; then
    sudo apt build-dep vim -y
    PYVER="python3.$(python3 --version | sed -E 's/Python 3.([0-9]+).*/\1/g')"
    sudo apt install "${PYVER}-dev"
    mkdir -p ${HOME}/git_tmp
    pushd ${HOME}/git_tmp
    git clone https://github.com/vim/vim
    cd vim
    ./configure --with-features=huge --enable-python3interp \
                --with-python3-config-dir=/usr/lib/python3.${PYVER}/config-3.${PYVER}-x86_64-linux-gnu
    make -j
    sudo make install
    cd ..
    popd

    # make a temp dir for vim temp files
    mkdir ${HOME}/temp

    # install vundle and vim plugins
    git clone https://github.com/VundleVim/Vundle.vim.git ${HOME}/.vim/bundle/Vundle.vim
    ln -s $(realpath ./setup/.vimrc) ${HOME}/.vimrc
    ln -s $(realpath ./setup/syntax) ${HOME}/.vim/syntax
    ln -s $(realpath ./setup/colors) ${HOME}/.vim/colors
    /usr/local/bin/vim -c PluginInstall -c qall!
    cp -rf ./setup/bundle ${HOME}/.vim/
    pushd ${HOME}/git_tmp
    git clone https://github.com/powerline/fonts.git --depth=1
    cd fonts
    ./install.sh
    cd ..
    popd
    rm -rf ${HOME}/git_tmp
fi

# install vim-related programs
install_if_needed universal-ctags
install_if_needed ripgrep
install_if_needed ack

# bash-env-home setup
sudo rm /bin/sh
sudo ln -s /bin/bash /bin/sh
if [ ! -h ${HOME}/.bash_aliases ]; then
    ln -s $(realpath ./setup/.bash_aliases) ${HOME}/.bash_aliases
fi
if [ ! -h ${HOME}/tools ]; then
    ln -s $(realpath ./setup/tools) ${HOME}/tools
fi
if [ ! -h ${HOME}/.ucfg_default ]; then
    ln -s $(realpath ./setup/.ucfg_default) ${HOME}/.ucfg_default
fi
TOOLPATH=$HOME/tools
TOOL_PATH_INSTALLED=$(echo $PATH | grep "$TOOLPATH")
if [ -z "$TOOL_PATH_INSTALLED" ]; then
    echo "export PATH=${PATH}:${TOOLPATH}" >> ${HOME}/.profile
    echo "export PATH=${PATH}:${TOOLPATH}" | sudo tee -a /etc/environment
fi

# disable ModemManager (messes with Quectel prods attached over USB)
sudo systemctl stop ModemManager
sudo systemctl disable ModemManager

# add user to dialout group (for non-sudo serial port access)
sudo usermod -aG dialout $USER

# add sudoers entries for commonly used tools which require root
if [ ! -f /etc/sudoers.d/QExplorer ]; then
    echo "${USER} ALL=NOPASSWD:SETENV:$HOME/tools/QExplorer" | (sudo su -c "EDITOR=\"tee\" visudo -f /etc/sudoers.d/QExplorer")
fi
if [ ! -f /etc/sudoers.d/QFirehose ]; then
    echo "${USER} ALL=NOPASSWD:SETENV:$HOME/tools/QFirehose" | (sudo su -c "EDITOR=\"tee\" visudo -f /etc/sudoers.d/QFirehose")
fi
if [ ! -f /etc/sudoers.d/QLog ]; then
    echo "${USER} ALL=NOPASSWD:SETENV:$HOME/tools/QLog" | (sudo su -c "EDITOR=\"tee\" visudo -f /etc/sudoers.d/QLog")
fi
if [ ! -f /etc/sudoers.d/cat ]; then
    echo "${USER} ALL=NOPASSWD:SETENV:/usr/bin/cat" | (sudo su -c "EDITOR=\"tee\" visudo -f /etc/sudoers.d/cat")
fi
if [ ! -f '/etc/sudoers.d/[' ]; then
    echo "${USER} ALL=NOPASSWD:SETENV:/usr/bin/[" | (sudo su -c "EDITOR=\"tee\" visudo -f /etc/sudoers.d/[")
fi

# build and install Quectel tools
if [ ! -f ${HOME}/tools/QExplorer ]; then
    cd ./qexplorer_src
    make
    mv QExplorer_* ${HOME}/tools/QExplorer
    cd ..
fi
if [ ! -f ${HOME}/tools/QFirehose ]; then
    cd ./qfirehose_src
    make
    mv QFirehose ${HOME}/tools/
    cd ..
fi
if [ ! -f ${HOME}/tools/QLog ]; then
    cd ./qlog_src
    make
    mv QLog ${HOME}/tools/
    cd ..
fi

# install docker
if [ -z $(which docker) ]; then
    install_if_needed ca-certificates
    install_if_needed gnupg
    install_if_needed lsb-release
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
        sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) \
        signed-by=/etc/apt/keyrings/docker.gpg] \
        https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt update
    install_if_needed docker-ce
    install_if_needed docker-ce-cli
    install_if_needed containerd.io
    install_if_needed docker-compose-plugin
    sudo groupadd docker
    sudo usermod -aG docker $USER
fi

# install swagger editor
SWAGGER_PULLED=$(sudo docker images | grep swagger)
if [ -z "$SWAGGER_PULLED" ]; then
    sudo docker pull swaggerapi/swagger-editor
fi

# install postman
if [ ! -d "$HOME/Postman" ]; then
    pushd $HOME
    curl https://dl.pstmn.io/download/latest/linux64 --output ./postman-linux-x64.tar.gz
    tar -xzf ./postman-linux-x64.tar.gz
    rm ./postman-linux-x64.tar.gz
    popd
    cp ./postman.env $HOME/Postman
    echo ". \"\$HOME/Postman/postman.env\"" >> ${HOME}/.profile
    echo ". \"\$HOME/Postman/postman.env\"" >> ${HOME}/.bashrc
fi

# install Poetry (python dependency management system)
if [ ! -f $HOME/.local/bin/poetry ]; then
    curl -sSL https://install.python-poetry.org | python3 -
    cp ./poetry.env $HOME/.local/share/pypoetry/
    echo ". \"\$HOME/.local/share/pypoetry/poetry.env\"" >> ${HOME}/.profile
    echo ". \"\$HOME/.local/share/pypoetry/poetry.env\"" >> ${HOME}/.bashrc
    $HOME/.local/bin/poetry completions bash >> ${HOME}/.bash_completion
fi



echo ""
echo ""
echo "main setup DONE, please reboot!"
