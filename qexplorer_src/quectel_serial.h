#ifndef QUECTEL_SERIAL_H
#define QUECTEL_SERIAL_H

#include "quectel_include.h"

#define HANDLE int

typedef struct
{
    char *port_name;
    HANDLE port_fd;
} com_port_t;

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))

void port_disconnect();
int port_connect(char *port_name);
int tx_data(byte *buff, size_t bytes_to_send);
int rx_data(byte *buff, size_t bytes_to_read, size_t *bytes_read);

#endif