#!/bin/bash

sudo chmod +x ./linux_vim_create.sh
sudo ./linux_vim_create.sh
sudo apt install universal-ctags -y
sudo apt install ripgrep -y
sudo apt install ack -y
sudo apt install curl -y
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
ln -s $(realpath ./.vimrc) ~/.vimrc
ln -s $(realpath ./syntax) ~/.vim/syntax
ln -s $(realpath ./colors) ~/.vim/colors
mkdir ~/temp
vim -c PluginInstall -c qall!
cp bundle ~/.vim/ -fr
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts
